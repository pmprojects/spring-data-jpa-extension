[![pipeline status](https://gitlab.com/pmprojects/spring-data-jpa-extension/badges/main/pipeline.svg)](https://gitlab.com/pmprojects/spring-data-jpa-extension/-/commits/main)
[![coverage report](https://gitlab.com/pmprojects/spring-data-jpa-extension/badges/main/coverage.svg)](https://gitlab.com/pmprojects/spring-data-jpa-extension/-/commits/main)
[![Latest Release](https://gitlab.com/pmprojects/spring-data-jpa-extension/-/badges/release.svg)](https://gitlab.com/pmprojects/spring-data-jpa-extension/-/releases)

This library provides a layer based on Spring Data JPA for complex data filtering and fields encryption

## Usage

```java
@Configuration
@EnableJpaRepositories(repositoryBaseClass = JpaExtendedRepositoryImpl.class)
public class Configuration {

    @PostConstruct //To enable encryption
    public void initEncryption() {
        CipherMakerConfig config = new CipherMakerConfig("MySuperSecretKey");
        CipherMaker.init(config);
    }
}
```

```java
@Entity
public class Customer {

    @Convert(converter = StringJpaCryptoConverter.class)
    private String sensitiveString;

    @Convert(converter = LocalDateJpaCryptoConverter.class)
    private LocalDate sensitiveLocalDate;

    @Convert(converter = IntegerJpaCryptoConverter.class)
    private Integer sensitiveInteger;

    @Convert(converter = LongJpaCryptoConverter.class)
    private Long sensitiveLong;

    @Convert(converter = DoubleJpaCryptoConverter.class)
    private Double sensitiveDouble;
}
```
```java
@Repository
public interface CustomerRepository extends JpaExtendedRepository<Customer, Long> {

}
```

```java
@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Page<Customer> method() {
        //Find first 10 customers, sorted by id, that have name equal to 'Customer name'
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.setLimit(10);
        queryConfig.addSort(DbSort.builder()
                .field(Customer_.ID)
                .direction(SortDir.ASC)
                .build());
        queryConfig.addFilter(DbFilterField.builder()
                .fieldName(DbFilterField.toAttributeName(Customer_.SENSITIVE_STRING))
                .operator(DbFilterOperator.EQUALS)
                .argument1("Customer name")
                .build());
        Page<Customer> customers = customerRepository.find(queryConfig);
        return customers;
    }
}
```

## Maven

```xml
<dependency>
    <groupId>com.pmprojects</groupId>
    <artifactId>spring-data-jpa-extension</artifactId>
    <version>0.0.1</version>
</dependency>
```

## Build

```shell
mvn package
```