package com.pmprojects.springdatajpaextension.repository;

import com.pmprojects.springdatajpaextension.domain.DbSort;
import com.pmprojects.springdatajpaextension.domain.DbSort.SortDir;
import com.pmprojects.springdatajpaextension.domain.JpaExtendedSpecification;
import com.pmprojects.springdatajpaextension.domain.OffsetBasedPageRequest;
import com.pmprojects.springdatajpaextension.domain.QueryConfig;
import com.pmprojects.springdatajpaextension.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class JpaExtendedRepositoryImpl<ENTITY, ID extends Serializable> extends SimpleJpaRepository<ENTITY, ID>
        implements JpaExtendedRepository<ENTITY, ID> {

    private final EntityManager entityManager;

    public JpaExtendedRepositoryImpl(JpaEntityInformation<ENTITY, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public Page<ENTITY> find(QueryConfig queryConfig) {
        try {
            JpaExtendedSpecification<ENTITY> spec = getSpecification(queryConfig);
            Sort sort = toSort(queryConfig.getSorts());
            if (queryConfig.getLimit() != null) {
                Pageable pageRequest = new OffsetBasedPageRequest(queryConfig.getOffset(), queryConfig.getLimit(), sort);
                return findAll(spec, pageRequest);
            } else {
                return new PageImpl<>(findAll(spec, sort));
            }
        } catch (RuntimeException e) {
            String errorMessage = "Filter failed: " + getDomainClass().getSimpleName();
            throw new RuntimeException(errorMessage, e);
        }
    }
    
    protected Sort toSort(List<DbSort> sorts) {
        if (CollectionUtils.isEmpty(sorts)) { return Sort.unsorted(); }
        List<Order> orders = new ArrayList<>();
        for (DbSort dbSort : sorts) {
            orders.add(dbSort.getDirection().equals(SortDir.ASC) ? Order.asc(dbSort.getField()) : Order.desc(dbSort.getField()));
        }
        return Sort.by(orders);
    }

    @Override
    public long count(QueryConfig queryConfig) {
        try {
            JpaExtendedSpecification<ENTITY> spec = getSpecification(queryConfig);
            return count(spec);
        } catch (RuntimeException e) {
            String errorMessage = "Count failed: " + getDomainClass().getSimpleName();
            throw new RuntimeException(errorMessage, e);
        }
    }

    @Override
    public <FIELD> List<FIELD> filterField(QueryConfig queryConfig, SingularAttribute<ENTITY, FIELD> selectionField) {
        try {
            CriteriaBuilder builder = entityManager.getCriteriaBuilder();
            CriteriaQuery<FIELD> criteria = builder.createQuery(selectionField.getJavaType());
            Root<ENTITY> from = criteria.from(getDomainClass());
            JpaExtendedSpecification<ENTITY> spec = getSpecification(queryConfig);
            criteria.select(from.get(selectionField));
            Map<String, QueryUtils.NamedPath> paths = new HashMap<>();
            criteria.distinct(spec.isDistinctRequired());
            criteria.where(spec.toPredicates(from, criteria, builder, paths));
            criteria.orderBy(QueryUtils.createOrders(builder, from, queryConfig.getSorts(), getDomainClass(), paths));
            TypedQuery<FIELD> typed = entityManager.createQuery(criteria);
            if (queryConfig.getLimit() != null) {
                typed.setFirstResult(queryConfig.getOffset());
                typed.setMaxResults(queryConfig.getLimit());
            }
            return typed.getResultList();
        } catch (RuntimeException e) {
            String errorMessage = "Filter field failed: " + getDomainClass().getSimpleName();
            throw new RuntimeException(errorMessage, e);
        }
    }
    
    protected JpaExtendedSpecification<ENTITY> getSpecification(QueryConfig queryConfig) {
       return new JpaExtendedSpecification<>(getDomainClass(), queryConfig);
    }
}