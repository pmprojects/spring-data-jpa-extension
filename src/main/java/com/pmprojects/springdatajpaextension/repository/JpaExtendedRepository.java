package com.pmprojects.springdatajpaextension.repository;

import com.pmprojects.springdatajpaextension.domain.QueryConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface JpaExtendedRepository<ENTITY, ID extends Serializable> extends JpaRepository<ENTITY, ID> {

    Page<ENTITY> find(QueryConfig queryConfig);

    long count(QueryConfig queryConfig);

    <FIELD> List<FIELD> filterField(QueryConfig queryConfig, SingularAttribute<ENTITY, FIELD> selectionField);
}