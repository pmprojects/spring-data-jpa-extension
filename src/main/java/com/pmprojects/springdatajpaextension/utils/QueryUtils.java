package com.pmprojects.springdatajpaextension.utils;

import com.pmprojects.springdatajpaextension.domain.DbFilter;
import com.pmprojects.springdatajpaextension.domain.DbFilterComposite;
import com.pmprojects.springdatajpaextension.domain.DbFilterField;
import com.pmprojects.springdatajpaextension.domain.DbSort;

import javax.persistence.Entity;
import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;

public class QueryUtils {

    public static <T> List<Order> createOrders(CriteriaBuilder builder, Root<T> root, List<DbSort> dbSorts, Class<T> clazz, Map<String, NamedPath> paths) {
        List<Order> orders = new ArrayList<>();
        for (DbSort dbSort : dbSorts) {
            Order order = createOrder(builder, root, dbSort, clazz, paths);
            orders.add(order);
        }
        return orders;
    }

    public static <T> Order createOrder(CriteriaBuilder builder, Root<T> root, DbSort dbSort, Class<T> clazz, Map<String, NamedPath> paths) {
        NamedPath namedPath = getOrCreateNamedPath(dbSort.getField(), root, clazz, paths);
        switch (dbSort.getDirection()) {
            case ASC:
                return builder.asc(namedPath.path);
            case DESC:
                return builder.desc(namedPath.path);
            default:
                throw new RuntimeException("SortDir instance not available: " + dbSort.getDirection());
        }
    }

    public static <T> Predicate[] createPredicates(CriteriaBuilder builder, Root<T> from, List<DbFilter> filters, Class<T> clazz, Map<String, NamedPath> paths) {
        Predicate[] predicates = new Predicate[filters.size()];
        for (DbFilter filter : filters) {
            Predicate predicate = createPredicate(builder, from, filter, clazz, paths);
            if (predicate != null) {
                predicates[filters.indexOf(filter)] = predicate;
            }
        }
        return predicates;
    }

    public static <T> Predicate createPredicate(CriteriaBuilder builder, Root<T> from, DbFilter filter, Class<T> clazz, Map<String, NamedPath> paths) {
        if (filter instanceof DbFilterField) {
            return createPredicate(builder, from, (DbFilterField) filter, clazz, paths);
        } else if (filter instanceof DbFilterComposite) {
            return createPredicate(builder, from, (DbFilterComposite) filter, clazz, paths);
        }
        throw new RuntimeException("DbField instance not available: " + filter.getClass());
    }

    private static <T> Predicate createPredicate(CriteriaBuilder builder, Root<T> from, DbFilterField filter, Class<T> clazz, Map<String, NamedPath> paths) {
        PredicateFields predicateFields = getPredicateFields(filter, from, clazz, paths);
        switch (filter.getOperator()) {
            case BETWEEN:
                return PredicateBuilder.toBetweenPredicate(predicateFields, builder);
            case CONTAINS:
                return PredicateBuilder.toContainPredicate(predicateFields, builder);
            case GREATER_THAN:
                return PredicateBuilder.toGreaterThanPredicate(predicateFields, builder);
            case GREATER_OR_EQUALS_THAN:
                return PredicateBuilder.toGreaterThanOrEqualToPredicate(predicateFields, builder);
            case LESS_THAN:
                return PredicateBuilder.toLessThanPredicate(predicateFields, builder);
            case LESS_OR_EQUALS_THAN:
                return PredicateBuilder.toLessThanOrEqualToPredicate(predicateFields, builder);
            case NOT_CONTAINS:
                return PredicateBuilder.toNotContainPredicate(predicateFields, builder);
            case IS_FALSE:
                return PredicateBuilder.toIsFalsePredicate(predicateFields, builder);
            case IS_TRUE:
                return PredicateBuilder.toIsTruePredicate(predicateFields, builder);
            case EQUALS:
                return PredicateBuilder.toEqualPredicate(predicateFields, builder);
            case NOT_EQUALS:
                return PredicateBuilder.toNotEqualPredicate(predicateFields, builder);
            case IN:
                return PredicateBuilder.toInPredicate(predicateFields, builder);
            case NOT_IN:
                return PredicateBuilder.toNotInPredicate(predicateFields, builder);
            case IS_NULL:
                return PredicateBuilder.toIsNullPredicate(predicateFields, builder);
            case IS_NOT_NULL:
                return PredicateBuilder.toIsNotNullPredicate(predicateFields, builder);
            case STARTS_WITH:
                return PredicateBuilder.toStartsWithPredicate(predicateFields, builder);
            case ENDS_WITH:
                return PredicateBuilder.toEndsWithPredicate(predicateFields, builder);
            default:
                throw new RuntimeException("DbFilterOperation instance not available: " + filter.getOperator());
        }
    }

    private static <T> Predicate createPredicate(CriteriaBuilder builder, Root<T> from, DbFilterComposite filter, Class<T> clazz, Map<String, NamedPath> paths) {
        List<Predicate> predicates = new ArrayList<>(filter.getFilters().size());
        for (DbFilter dbFilter : filter.getFilters()) {
            predicates.add(createPredicate(builder, from, dbFilter, clazz, paths));
        }
        switch (filter.getOperator()) {
            case AND:
                return builder.and(predicates.toArray(new Predicate[predicates.size()]));
            case OR:
                return builder.or(predicates.toArray(new Predicate[predicates.size()]));
        }
        throw new RuntimeException("BooleanOperator instance not available: " + filter.getOperator());
    }

    public static List<String> getFieldNameList(String fieldName) {
        List<String> fieldNames = new ArrayList<>();
        if (!fieldName.isEmpty()) {
            Collections.addAll(fieldNames, fieldName.split("\\" + AttributeUtils.FIELDNAME_SEPARATOR));
        }
        return fieldNames;
    }

    private static <T> Field getField(Class<T> clazz, String fieldName) {
        try {
            if (clazz == null) {
                return null;
            }
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            return getField(clazz.getSuperclass(), fieldName);
        }
    }

    private static <T> PredicateFields getPredicateFields(DbFilterField filter, Root<T> from, Class<T> clazz, Map<String, NamedPath> paths) {
        String fieldName = filter.getFieldName();
        NamedPath namedPath = getOrCreateNamedPath(fieldName, from, clazz, paths);
        return new PredicateFields(namedPath.path, filter.getArgument1(), filter.getArgument2(), namedPath.clazz);
    }

    private static <T> NamedPath getOrCreateNamedPath(String fieldName, Root<T> from, Class<T> clazz, Map<String, NamedPath> paths) {
        try {
            Path<?> path = from;
            Class<?> fieldClazz = clazz;
            Class<?> fieldClazzOld = clazz;
            String pathName = "";
            NamedPath namedPath = findNamedPath(paths, fieldName);
            if (namedPath != null) {
                path = namedPath.path;
                fieldClazz = namedPath.clazz;
                fieldClazzOld = namedPath.clazz;
                pathName = namedPath.pathName;
                fieldName = fieldName.replace(namedPath.pathName, "");
                if (fieldName.startsWith(AttributeUtils.FIELDNAME_SEPARATOR)) {
                    fieldName = fieldName.replaceFirst(AttributeUtils.FIELDNAME_SEPARATOR, "");
                }
            }
            List<String> fieldNames = getFieldNameList(fieldName);
            NamedPath newNamedPath = namedPath;
            for (String name : fieldNames) {
                if (!pathName.isEmpty()) {
                    pathName += AttributeUtils.FIELDNAME_SEPARATOR;
                }
                pathName += name;
                Field field = getField(fieldClazz, name);
                fieldClazz = field.getType();
                if (Collection.class.isAssignableFrom(fieldClazz)) {
                    Field fieldCollection = fieldClazzOld.getDeclaredField(name);
                    ParameterizedType fieldType = (ParameterizedType) fieldCollection.getGenericType();
                    fieldClazz = (Class<?>) fieldType.getActualTypeArguments()[0];
                }
                if (Map.class.isAssignableFrom(fieldClazz)) {
                    Field fieldMap = fieldClazzOld.getDeclaredField(name);
                    ParameterizedType fieldType = (ParameterizedType) fieldMap.getGenericType();
                    fieldClazz = (Class<?>) fieldType.getActualTypeArguments()[1];
                }

                if (fieldClazz.isAnnotationPresent(Entity.class)) {
                    // For common use of filters Left Join is enough. Custom behaviors need custom implementations
                    path = ((From<?, ?>) path).join(name, JoinType.LEFT);
                } else {
                    path = path.get(name);
                }
                fieldClazzOld = fieldClazz;

                newNamedPath = new NamedPath(path, fieldClazz, pathName);
                paths.put(pathName, newNamedPath);
            }
            return newNamedPath;
        } catch (NoSuchFieldException | SecurityException e) {
            throw new RuntimeException("", e);
        }
    }

    private static NamedPath findNamedPath(Map<String, NamedPath> paths, String fieldName) {
        if (paths.containsKey(fieldName)) {
            NamedPath namedPath = paths.get(fieldName);
            return namedPath;
        }
        List<String> fieldNames = getFieldNameList(fieldName);
        fieldNames = fieldNames.subList(0, fieldNames.size() - 1);
        if (fieldNames.isEmpty()) {
            return null;
        }
        return findNamedPath(paths, String.join(AttributeUtils.FIELDNAME_SEPARATOR, fieldNames));
    }

    public static <T> boolean isFilterCollectionPresent(List<DbFilter> filters, Class<T> clazz) {
        return filters.stream().anyMatch(filter -> isFilterCollectionPresent(filter, clazz));
    }

    public static <T> boolean isFilterCollectionPresent(DbFilter filter, Class<T> clazz) {
        if (filter instanceof DbFilterComposite) {
            return isFilterCollectionPresent(((DbFilterComposite) filter).getFilters(), clazz);
        } else if (filter instanceof DbFilterField) {
            return isCollectionField(((DbFilterField) filter).getFieldName(), clazz);
        }
        return false;
    }

    public static <T> boolean isSortCollectionPresent(List<DbSort> sorts, Class<T> clazz) {
        return sorts.stream().anyMatch(sort -> isSortCollectionPresent(sort, clazz));
    }

    public static <T> boolean isSortCollectionPresent(DbSort sort, Class<T> clazz) {
        return isCollectionField(sort.getField(), clazz);
    }

    private static <T> boolean isCollectionField(String pathField, Class<T> clazz) {
        List<String> fieldNames = getFieldNameList(pathField);
        Class<?> fieldClazz = clazz;
        for (String fieldName : fieldNames) {
            Field field = getField(fieldClazz, fieldName);
            fieldClazz = field.getType();
            if (!Collection.class.isAssignableFrom(fieldClazz) && !Map.class.isAssignableFrom(fieldClazz)) {
                continue;
            }
            return true;
        }
        return false;
    }

    public static class PredicateFields {
        Path<?> path;
        Object field1;
        Object field2;
        Class<?> clazzField;
        
        public PredicateFields(Path<?> path, Object field1, Object field2, Class<?> clazzField) {
            super();
            this.path = path;
            this.field1 = field1;
            this.field2 = field2;
            this.clazzField = clazzField;
        }
    }

    public static class NamedPath {
        Path<?> path;
        Class<?> clazz;
        String pathName;

        public NamedPath(Path<?> path, Class<?> clazz, String pathName) {
            this.path = path;
            this.clazz = clazz;
            this.pathName = pathName;
        }
    }
}
