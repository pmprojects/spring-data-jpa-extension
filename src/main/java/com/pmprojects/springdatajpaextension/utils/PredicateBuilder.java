package com.pmprojects.springdatajpaextension.utils;

import com.pmprojects.springdatajpaextension.utils.QueryUtils.PredicateFields;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.util.Collection;
import java.util.stream.Collectors;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class PredicateBuilder {

    private static final String LIKE_WILDCARD = "%";
    
    public static Predicate toBetweenPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        if (Number.class.isAssignableFrom(predicateFields.clazzField)) {
            return builder.and(builder.ge((Expression<? extends Number>) predicateFields.path, (Number) predicateFields.field1),
                    builder.le((Expression<? extends Number>) predicateFields.path, (Number) predicateFields.field2));
        }
        return builder.between((Expression<? extends Comparable>) predicateFields.path, (Comparable) predicateFields.field1, (Comparable) predicateFields.field2);
    }

    public static Predicate toGreaterThanOrEqualToPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        if (Number.class.isAssignableFrom(predicateFields.clazzField)) {
            return builder.ge((Expression<? extends Number>) predicateFields.path, (Number) predicateFields.field1);
        }
        return builder.greaterThanOrEqualTo((Expression<? extends Comparable>) predicateFields.path, (Comparable) predicateFields.field1);
    }

    public static Predicate toLessThanOrEqualToPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        if (Number.class.isAssignableFrom(predicateFields.clazzField)) {
            return builder.le((Expression<? extends Number>) predicateFields.path, (Number) predicateFields.field1);
        }
        return builder.lessThanOrEqualTo((Expression<? extends Comparable>) predicateFields.path, (Comparable) predicateFields.field1);
    }

    public static Predicate toGreaterThanPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        if (Number.class.isAssignableFrom(predicateFields.clazzField)) {
            return builder.gt((Expression<? extends Number>) predicateFields.path, (Number) predicateFields.field1);
        }
        return builder.greaterThan((Expression<? extends Comparable>) predicateFields.path, (Comparable) predicateFields.field1);
    }

    public static Predicate toLessThanPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        if (Number.class.isAssignableFrom(predicateFields.clazzField)) {
            return builder.lt((Expression<? extends Number>) predicateFields.path, (Number) predicateFields.field1);
        }
        return builder.lessThan((Expression<? extends Comparable>) predicateFields.path, (Comparable) predicateFields.field1);
    }
    
    public static Predicate toContainPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        return builder.like(predicateFields.path.as(String.class), LIKE_WILDCARD + predicateFields.field1 + LIKE_WILDCARD);
    }
    
    public static Predicate toNotContainPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        return builder.notLike(predicateFields.path.as(String.class), LIKE_WILDCARD + predicateFields.field1 + LIKE_WILDCARD);
    }
    
    public static Predicate toIsTruePredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        return builder.isTrue((Expression<Boolean>) predicateFields.path);
    }
    
    public static Predicate toIsFalsePredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        return builder.isFalse((Expression<Boolean>) predicateFields.path);
    }
    
    public static Predicate toEqualPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        if (predicateFields.clazzField.isEnum() && !(predicateFields.field1 instanceof Enum<?>)) {
            predicateFields.field1 = Enum.valueOf((Class<Enum>) predicateFields.clazzField, predicateFields.field1.toString());
        }
        return builder.equal(predicateFields.path, predicateFields.field1);
    }
    
    public static Predicate toNotEqualPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        if (predicateFields.clazzField.isEnum() && !(predicateFields.field1 instanceof Enum<?>)) {
            predicateFields.field1 = Enum.valueOf((Class<Enum>) predicateFields.clazzField, predicateFields.field1.toString());
        }
        return builder.notEqual(predicateFields.path, predicateFields.field1);
    }
    
    public static Predicate toInPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        Collection<?> field = (Collection<?>) predicateFields.field1;
        if (predicateFields.clazzField.isEnum() && field.stream().noneMatch(fieldElement -> fieldElement instanceof Enum<?>)) {
            predicateFields.field1 = field.stream().map(fieldElement -> Enum.valueOf((Class<Enum>) predicateFields.clazzField, fieldElement.toString())).collect(Collectors.toList());  
        }
        return predicateFields.path.in((Collection<?>) predicateFields.field1);
    }
    
    public static Predicate toNotInPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
    	return toInPredicate(predicateFields, builder).not();
    }
    
    public static Predicate toIsNullPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        return builder.isNull(predicateFields.path);
    }
    
    public static Predicate toIsNotNullPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        return builder.isNotNull(predicateFields.path);
    }
    
    public static Predicate toStartsWithPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        return builder.like(predicateFields.path.as(String.class), predicateFields.field1 + LIKE_WILDCARD);
    }
    
    public static Predicate toEndsWithPredicate(PredicateFields predicateFields, CriteriaBuilder builder) {
        return builder.like(predicateFields.path.as(String.class), (LIKE_WILDCARD + predicateFields.field1));
    }
}
