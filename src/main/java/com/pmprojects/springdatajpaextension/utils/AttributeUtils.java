package com.pmprojects.springdatajpaextension.utils;

import javax.persistence.metamodel.Attribute;
import java.util.ArrayList;
import java.util.List;

public class AttributeUtils {

    public static final String FIELDNAME_SEPARATOR = ".";
    
    public static String toAttributeName(Attribute<?, ?>... singularAttributes) {
        return toAttributeName(toAttributeNames(singularAttributes));
    }
    
    public static String toAttributeName(String... attributes) {
        StringBuilder attributeName = new StringBuilder();
        for (String attribute : attributes) {
            if (attributeName.length() > 0) {
                attributeName.append(FIELDNAME_SEPARATOR);
            }
            attributeName.append(attribute);
        }
        return attributeName.toString();
    }
    
    private static String[] toAttributeNames(Attribute<?, ?>... singularAttributes) {
        List<String> attributeNames = new ArrayList<>(singularAttributes.length);
        for (Attribute<?, ?> singularAttribute : singularAttributes) {
            attributeNames.add(singularAttribute.getName());
        }
        return attributeNames.toArray(new String[attributeNames.size()]);
    }
}
