package com.pmprojects.springdatajpaextension.domain;

public enum DbFilterOperator {
    EQUALS,
    NOT_EQUALS,
    GREATER_THAN,
    GREATER_OR_EQUALS_THAN,
    LESS_THAN,
    LESS_OR_EQUALS_THAN,
    CONTAINS,
    NOT_CONTAINS,
    IS_TRUE,
    IS_FALSE,
    BETWEEN,
    IN,
    NOT_IN,
    IS_NULL,
    IS_NOT_NULL,
    STARTS_WITH,
    ENDS_WITH
}
