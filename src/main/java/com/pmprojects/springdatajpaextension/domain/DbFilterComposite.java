package com.pmprojects.springdatajpaextension.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.criteria.Predicate.BooleanOperator;
import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
@ToString
public class DbFilterComposite implements DbFilter {

    private BooleanOperator operator;
    @Singular(value = "filter")
    private List<DbFilter> filters;

    @JsonCreator
    public DbFilterComposite(@JsonProperty(value = "operator", required = true) BooleanOperator operator, @JsonProperty(value = "filters", required = true) List<DbFilter> filters) {
       this.operator = operator;
       this.filters = filters;
    }

    public DbFilterComposite(BooleanOperator operator) {
        this(operator, new ArrayList<>());
    }

    public void addFilter(DbFilter filter) {
        filters.add(filter);
    }
}
