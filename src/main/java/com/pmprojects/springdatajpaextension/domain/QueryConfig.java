package com.pmprojects.springdatajpaextension.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class QueryConfig {

	@Singular
    private List<DbFilter> filters = new ArrayList<>();
	@Singular
    private List<DbSort> sorts = new ArrayList<>();
    private Integer limit;
    @Builder.Default
    private int offset = 0;

    public static QueryConfigBuilder builder() {
        return new QueryConfigBuilder()
        		.filters(new ArrayList<>())
        		.sorts(new ArrayList<>());
    }
    
    public void addFilter(DbFilter filter) {
        filters.add(filter);
    }

    public void addSort(DbSort sort) {
        sorts.add(sort);
    }
}
