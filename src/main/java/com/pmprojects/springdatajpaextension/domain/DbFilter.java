package com.pmprojects.springdatajpaextension.domain;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type",
        defaultImpl = DbFilterField.class)
@JsonSubTypes({
        @JsonSubTypes.Type(value = DbFilterField.class, name = "field"),
        @JsonSubTypes.Type(value = DbFilterComposite.class, name = "composite")
})
public interface DbFilter {

}
