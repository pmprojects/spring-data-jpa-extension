package com.pmprojects.springdatajpaextension.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class DbFilterField implements DbFilter {

    @JsonProperty(required = true)
    private String fieldName;
    @JsonProperty(required = true)
    private DbFilterOperator operator;
    private Object argument1;
    private Object argument2;
    
    public DbFilterField(String fieldName, DbFilterOperator operator, Object argument1) {
        this(fieldName, operator, argument1, null);
    }

    public DbFilterField(String fieldName, DbFilterOperator operator) {
        this(fieldName, operator, null);
    }
}
