package com.pmprojects.springdatajpaextension.domain;

import com.pmprojects.springdatajpaextension.domain.DbFilter;
import com.pmprojects.springdatajpaextension.domain.DbSort;
import com.pmprojects.springdatajpaextension.domain.QueryConfig;
import com.pmprojects.springdatajpaextension.utils.QueryUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
@AllArgsConstructor
public class JpaExtendedSpecification<ENTITY> implements Specification<ENTITY> {

    protected Class<ENTITY> clazz;
    protected List<DbFilter> filters;
    protected List<DbSort> sorts;

    public JpaExtendedSpecification(Class<ENTITY> clazz, QueryConfig queryConfig) {
        this(clazz, queryConfig.getFilters(), queryConfig.getSorts());
    }

    public Predicate[] toPredicates(Root<ENTITY> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return toPredicates(root, query, criteriaBuilder, new HashMap<>());
    }

    public Predicate[] toPredicates(Root<ENTITY> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, Map<String, QueryUtils.NamedPath> paths) {
        return QueryUtils.createPredicates(criteriaBuilder, root, filters, clazz, paths);
    }

    @Override
    public Predicate toPredicate(Root<ENTITY> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        query.distinct(isDistinctRequired());
        Predicate predicate = criteriaBuilder.and(toPredicates(root, query, criteriaBuilder));
        return predicate;
    }

    public boolean isDistinctRequired() {
        boolean distinctRequired = false;
        if (QueryUtils.isFilterCollectionPresent(filters, clazz) || QueryUtils.isSortCollectionPresent(sorts, clazz)) {
            distinctRequired = true;
        }
        return distinctRequired;
    }
}
