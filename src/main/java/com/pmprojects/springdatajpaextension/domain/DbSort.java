package com.pmprojects.springdatajpaextension.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class DbSort implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(required = true)
    private String field;
    @Builder.Default
    private SortDir direction = SortDir.ASC;

    public enum SortDir {
        ASC,
        DESC
    }
}
