package com.pmprojects.springdatajpaextension.crypto.converter;

public class LongJpaCryptoConverter extends NumberJpaCryptoConverter<Long> {

    @Override
    Long convertStringToEntityAttribute(String dbData) {
        return ((dbData == null) ? null : Long.parseLong(dbData));
    }

}