package com.pmprojects.springdatajpaextension.crypto.converter;

import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateJpaCryptoConverter extends JpaCryptoConverter<LocalDate> {

    @Override
    boolean isNotNullOrEmpty(LocalDate attribute) {
        return attribute != null;
    }

    @Override
    LocalDate convertStringToEntityAttribute(String dbData) {
        return StringUtils.hasLength(dbData) ? LocalDate.parse(dbData, DateTimeFormatter.ISO_DATE) : null;
    }

    @Override
    String convertEntityAttributeToString(LocalDate attribute) {
        return ((attribute == null) ? null : attribute.format(DateTimeFormatter.ISO_DATE));
    }
}
