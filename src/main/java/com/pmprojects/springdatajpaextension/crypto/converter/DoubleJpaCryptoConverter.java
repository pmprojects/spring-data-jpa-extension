package com.pmprojects.springdatajpaextension.crypto.converter;

public class DoubleJpaCryptoConverter extends NumberJpaCryptoConverter<Double> {

    @Override
    Double convertStringToEntityAttribute(String dbData) {
        return ((dbData == null) ? null : Double.parseDouble(dbData));
    }

}
