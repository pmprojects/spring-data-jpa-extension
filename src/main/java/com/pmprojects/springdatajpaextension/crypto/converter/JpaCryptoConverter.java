package com.pmprojects.springdatajpaextension.crypto.converter;

import com.pmprojects.springdatajpaextension.crypto.CipherMaker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Converter
@Slf4j
public abstract class JpaCryptoConverter<X> implements AttributeConverter<X, String> {

    /*
     * (non-Javadoc)
     * 
     * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.Object)
     */
    @Override
    public String convertToDatabaseColumn(X attribute) {
        if (isNotNullOrEmpty(attribute)) {
            try {
                Cipher cipher = CipherMaker.getEncryptCipher();
                return encryptData(cipher, attribute);
            } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException | BadPaddingException
                    | NoSuchPaddingException | IllegalBlockSizeException e) {
                throw new RuntimeException(e);
            }
        }
        return convertEntityAttributeToString(attribute);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang. Object)
     */
    @Override
    public X convertToEntityAttribute(String dbData) {
        if (StringUtils.hasLength(dbData)) {
            try {
                Cipher cipher = CipherMaker.getDecryptCipher();
                return decryptData(cipher, dbData);
            } catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidAlgorithmParameterException
                    | IllegalBlockSizeException | BadPaddingException e) {
                log.error("Error during data decryption", e);
                throw new RuntimeException(e);
            }
        }
        return convertStringToEntityAttribute(dbData);
    }

    /**
     * The concrete class which implements this abstract class will have to provide the implementation.
     * For simple String encryption, the implementation is simple as apache commons lang StringUtils can
     * be used. But this method was abstracted out as there might be other types of null check technique
     * required when a non String entity is to be encrypted
     * 
     * @param attribute
     * @return
     */
    abstract boolean isNotNullOrEmpty(X attribute);

    /**
     * The concrete class which implements this abstract class will have to provide the implementation.
     * For decryption of a String, its simple as a String has to be returned, but for other non String
     * types some more code might have to be implemented. For example, a Date type of Date string.
     * 
     * @param dbData
     * @return
     */
    abstract X convertStringToEntityAttribute(String dbData);

    /**
     * The concrete class which implements this abstract class will have to provide the implementation.
     * For encryption of a String, its simple as a String has to be returned, but for other non String
     * types some more code might have to be implemented. For example, a Date type of Date string.
     * 
     * @param attribute
     * @return
     */
    abstract String convertEntityAttributeToString(X attribute);

    /**
     * Helper method to encrypt data
     * 
     * @param cipher
     * @param attribute
     * @return
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private String encryptData(Cipher cipher, X attribute) throws IllegalBlockSizeException, BadPaddingException {
        byte[] bytesToEncrypt = convertEntityAttributeToString(attribute).getBytes();
        byte[] encryptedBytes = cipher.doFinal(bytesToEncrypt);
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    /**
     * Helper method to decrypt data
     * 
     * @param cipher
     * @param dbData
     * @return
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private X decryptData(Cipher cipher, String dbData) throws IllegalBlockSizeException, BadPaddingException {
        byte[] bytesToDecrypt = Base64.getDecoder().decode(dbData);
        byte[] decryptedBytes = cipher.doFinal(bytesToDecrypt);
        return convertStringToEntityAttribute(new String(decryptedBytes));
    }
}