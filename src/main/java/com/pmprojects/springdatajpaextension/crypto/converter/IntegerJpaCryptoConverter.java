package com.pmprojects.springdatajpaextension.crypto.converter;

public class IntegerJpaCryptoConverter extends NumberJpaCryptoConverter<Integer> {

    @Override
    Integer convertStringToEntityAttribute(String dbData) {
        return ((dbData == null) ? null : Integer.parseInt(dbData));
    }

}
