package com.pmprojects.springdatajpaextension.crypto.converter;

public abstract class NumberJpaCryptoConverter<X extends Number> extends JpaCryptoConverter<X> {

    @Override
    boolean isNotNullOrEmpty(X attribute) {
        return attribute != null;
    }

    @Override
    String convertEntityAttributeToString(X attribute) {
        return ((attribute == null) ? null : attribute.toString());
    }

}
