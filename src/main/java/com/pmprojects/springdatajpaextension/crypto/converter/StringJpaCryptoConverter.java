package com.pmprojects.springdatajpaextension.crypto.converter;

import org.springframework.util.StringUtils;

public class StringJpaCryptoConverter extends JpaCryptoConverter<String> {

    @Override
    boolean isNotNullOrEmpty(String attribute) {
        return StringUtils.hasLength(attribute);
    }

    @Override
    String convertStringToEntityAttribute(String dbData) {
        // the input is a string and output is a string
        return dbData;
    }

    @Override
    String convertEntityAttributeToString(String attribute) {
        // Here too the input is a string and output is a string
        return attribute;
    }
}
