package com.pmprojects.springdatajpaextension.crypto;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Objects;

public class CipherMaker {

    private final CipherMakerConfig config;

    private static CipherMaker _instance;

    private CipherMaker(CipherMakerConfig config) {
        Objects.requireNonNull(config, "CipherMakerConfig cannot be null");
        this.config = config;
    }

    public Cipher configureAndGetInstance(int encryptionMode)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        Cipher cipher = Cipher.getInstance(config.getTransformation());
        Key secretKey = config.getKey();

        byte[] ivBytes = new byte[cipher.getBlockSize()];
        AlgorithmParameterSpec algorithmParameters = new IvParameterSpec(ivBytes);

        cipher.init(encryptionMode, secretKey, algorithmParameters);
        return cipher;
    }

    public static void init(CipherMakerConfig config) {
        _instance = new CipherMaker(config);
    }

    public static CipherMaker getInstance() {
        return _instance;
    }

    public static Cipher getEncryptCipher()
            throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        return getInstance().configureAndGetInstance(Cipher.ENCRYPT_MODE);
    }
    
    public static Cipher getDecryptCipher()
            throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        return getInstance().configureAndGetInstance(Cipher.DECRYPT_MODE);
    }
}
