package com.pmprojects.springdatajpaextension.crypto;

import lombok.Getter;
import lombok.Setter;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

@Setter
@Getter
public class CipherMakerConfig {

    private String transformation;
    private String algorithm;
    private Key key;

    public CipherMakerConfig(String key) {
        this(key.getBytes());
    }
    
    public CipherMakerConfig(byte[] keyBytes) {
        this("AES/CBC/PKCS5Padding", "AES", keyBytes);
    }
    
    public CipherMakerConfig(String transformation, String algorithm, byte[] keyBytes) {
        this.transformation = transformation;
        this.algorithm = algorithm;
        this.key = new SecretKeySpec(keyBytes, algorithm);
    }

}
