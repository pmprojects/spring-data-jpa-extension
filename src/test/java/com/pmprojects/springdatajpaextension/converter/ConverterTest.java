package com.pmprojects.springdatajpaextension.converter;

import com.pmprojects.springdatajpaextension.crypto.CipherMaker;
import com.pmprojects.springdatajpaextension.crypto.CipherMakerConfig;
import com.pmprojects.springdatajpaextension.entity.SensitiveData;
import com.pmprojects.springdatajpaextension.entity.SensitiveData_;
import com.pmprojects.springdatajpaextension.repository.JpaExtendedRepositoryImpl;
import com.pmprojects.springdatajpaextension.repository.SensitiveDataRepository;
import com.pmprojects.springdatajpaextension.domain.DbFilterField;
import com.pmprojects.springdatajpaextension.domain.DbFilterOperator;
import com.pmprojects.springdatajpaextension.domain.QueryConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.crypto.spec.SecretKeySpec;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@EnableJpaRepositories(basePackages = { "com.pmprojects.springdatajpaextension.repository" }, repositoryBaseClass = JpaExtendedRepositoryImpl.class)
public class ConverterTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private SensitiveDataRepository sensitiveDataRepository;

    @BeforeEach
    public void setUp() {
        CipherMakerConfig config = new CipherMakerConfig("MySuperSecretKey");
        CipherMaker.init(config);
    }

    @Test
    public void testCipherMakerConfig() {
        CipherMakerConfig config1 = new CipherMakerConfig("MySuperSecretKey");

        CipherMakerConfig config2 = new CipherMakerConfig(config1.getKey().getEncoded());
        config2.setAlgorithm(config1.getAlgorithm());
        config2.setTransformation(config1.getTransformation());
        config2.setKey(new SecretKeySpec(config1.getKey().getEncoded(), config1.getAlgorithm()));

        assertEquals(config1.getKey(), config2.getKey());
        assertEquals(config1.getAlgorithm(), config2.getAlgorithm());
        assertEquals(config1.getTransformation(), config2.getTransformation());
    }

    @Test
    public void testCipherMakerThrow() {
        assertThrows(RuntimeException.class, () -> CipherMaker.init(null));
    }

    @Test
    public void stringConverter() {
        String sensitiveString = "sensitiveString";
        SensitiveData data = new SensitiveData();
        data.setId(1L);
        data.setSensitiveString(sensitiveString);
        sensitiveDataRepository.save(data);

        entityManager.flush();
        entityManager.clear();

        SensitiveData dataSaved = sensitiveDataRepository.findById(1L).get();
        System.out.println(dataSaved);
        assertEquals(data.getSensitiveString(), dataSaved.getSensitiveString());

        entityManager.flush();
        entityManager.clear();

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(SensitiveData_.SENSITIVE_STRING, DbFilterOperator.EQUALS, sensitiveString));
        List<SensitiveData> sensitiveDatas = sensitiveDataRepository.find(queryConfig).getContent();
        assertFalse(sensitiveDatas.isEmpty());
        for (SensitiveData sensitiveData : sensitiveDatas) {
            assertEquals(data.getSensitiveString(), sensitiveData.getSensitiveString());
            assertNull(data.getSensitiveDouble());
            assertNull(data.getSensitiveInteger());
            assertNull(data.getSensitiveLong());
            assertNull(data.getSensitiveLocalDate());
        }
    }
    
    @Test
    public void localeDateConverter() {
        LocalDate sensitiveLocalDate = LocalDate.now();
        SensitiveData data = new SensitiveData();
        data.setId(1L);
        data.setSensitiveLocalDate(sensitiveLocalDate);
        sensitiveDataRepository.save(data);

        entityManager.flush();
        entityManager.clear();

        SensitiveData dataSaved = sensitiveDataRepository.findById(1L).get();
        System.out.println(dataSaved);
        assertEquals(data.getSensitiveLocalDate().toString(), dataSaved.getSensitiveLocalDate().toString());

        entityManager.flush();
        entityManager.clear();

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(SensitiveData_.SENSITIVE_LOCAL_DATE, DbFilterOperator.EQUALS, sensitiveLocalDate));
        List<SensitiveData> sensitiveDatas = sensitiveDataRepository.find(queryConfig).getContent();
        assertFalse(sensitiveDatas.isEmpty());
        for (SensitiveData sensitiveData : sensitiveDatas) {
            assertEquals(data.getSensitiveLocalDate().toString(), sensitiveData.getSensitiveLocalDate().toString());
            assertNull(data.getSensitiveDouble());
            assertNull(data.getSensitiveInteger());
            assertNull(data.getSensitiveString());
            assertNull(data.getSensitiveLong());
        }
    }
    
    @Test
    public void longConverter() {
        Long sensitiveLong = 100L;
        SensitiveData data = new SensitiveData();
        data.setId(1L);
        data.setSensitiveLong(sensitiveLong);
        sensitiveDataRepository.save(data);

        entityManager.flush();
        entityManager.clear();

        SensitiveData dataSaved = sensitiveDataRepository.findById(1L).get();
        System.out.println(dataSaved);
        assertEquals(data.getSensitiveLong(), dataSaved.getSensitiveLong());
        assertNull(data.getSensitiveDouble());
        assertNull(data.getSensitiveInteger());
        assertNull(data.getSensitiveString());

        entityManager.flush();
        entityManager.clear();

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(SensitiveData_.SENSITIVE_LONG, DbFilterOperator.EQUALS, sensitiveLong));
        List<SensitiveData> sensitiveDatas = sensitiveDataRepository.find(queryConfig).getContent();
        assertFalse(sensitiveDatas.isEmpty());
        for (SensitiveData sensitiveData : sensitiveDatas) {
            assertEquals(data.getSensitiveLong(), sensitiveData.getSensitiveLong());
            assertNull(data.getSensitiveDouble());
            assertNull(data.getSensitiveInteger());
            assertNull(data.getSensitiveString());
            assertNull(data.getSensitiveLocalDate());
        }
    }
    
    @Test
    public void integerConverter() {
        Integer sensitiveInteger = 100;
        SensitiveData data = new SensitiveData();
        data.setId(1L);
        data.setSensitiveInteger(sensitiveInteger);
        sensitiveDataRepository.save(data);

        entityManager.flush();
        entityManager.clear();

        SensitiveData dataSaved = sensitiveDataRepository.findById(1L).get();
        System.out.println(dataSaved);
        assertEquals(data.getSensitiveInteger(), dataSaved.getSensitiveInteger());
        assertNull(data.getSensitiveDouble());
        assertNull(data.getSensitiveLong());
        assertNull(data.getSensitiveString());

        entityManager.flush();
        entityManager.clear();

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(SensitiveData_.SENSITIVE_INTEGER, DbFilterOperator.EQUALS, sensitiveInteger));
        List<SensitiveData> sensitiveDatas = sensitiveDataRepository.find(queryConfig).getContent();
        assertFalse(sensitiveDatas.isEmpty());
        for (SensitiveData sensitiveData : sensitiveDatas) {
            assertEquals(data.getSensitiveInteger(), sensitiveData.getSensitiveInteger());
            assertNull(data.getSensitiveDouble());
            assertNull(data.getSensitiveLong());
            assertNull(data.getSensitiveString());
            assertNull(data.getSensitiveLocalDate());
        }
    }
    
    @Test
    public void doubleConverter() {
        Double sensitiveDouble = 100.100D;
        SensitiveData data = new SensitiveData();
        data.setId(1L);
        data.setSensitiveDouble(sensitiveDouble);
        sensitiveDataRepository.save(data);

        entityManager.flush();
        entityManager.clear();

        SensitiveData dataSaved = sensitiveDataRepository.findById(1L).get();
        System.out.println(dataSaved);
        assertEquals(data.getSensitiveDouble(), dataSaved.getSensitiveDouble());
        assertNull(data.getSensitiveInteger());
        assertNull(data.getSensitiveLong());
        assertNull(data.getSensitiveString());

        entityManager.flush();
        entityManager.clear();

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(SensitiveData_.SENSITIVE_DOUBLE, DbFilterOperator.EQUALS, sensitiveDouble));
        List<SensitiveData> sensitiveDatas = sensitiveDataRepository.find(queryConfig).getContent();
        assertFalse(sensitiveDatas.isEmpty());
        for (SensitiveData sensitiveData : sensitiveDatas) {
            assertEquals(data.getSensitiveDouble(), sensitiveData.getSensitiveDouble());
            assertNull(data.getSensitiveInteger());
            assertNull(data.getSensitiveLong());
            assertNull(data.getSensitiveString());
            assertNull(data.getSensitiveLocalDate());
        }
    }
}
