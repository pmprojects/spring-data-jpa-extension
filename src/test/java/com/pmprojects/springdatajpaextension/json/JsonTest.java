package com.pmprojects.springdatajpaextension.json;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import javax.persistence.criteria.Predicate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pmprojects.springdatajpaextension.domain.DbFilterComposite;
import com.pmprojects.springdatajpaextension.domain.DbFilterField;
import com.pmprojects.springdatajpaextension.domain.DbFilterOperator;
import com.pmprojects.springdatajpaextension.domain.DbSort;
import com.pmprojects.springdatajpaextension.domain.QueryConfig;
import com.pmprojects.springdatajpaextension.entity.Customer_;

public class JsonTest {

    private ObjectMapper mapper;

    @BeforeEach
    public void setup() {
        mapper = new ObjectMapper();
    }

    @Test
    public void testQueryConfigRequest() throws IOException {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.NAME, DbFilterOperator.IS_NULL));
        queryConfig.addFilter(new DbFilterComposite(Predicate.BooleanOperator.OR, Collections.singletonList(new DbFilterField(Customer_.DELETED, DbFilterOperator.IS_FALSE))));
        queryConfig.addFilter(new DbFilterComposite(Predicate.BooleanOperator.AND, Collections.singletonList(new DbFilterField(Customer_.DELETED, DbFilterOperator.IS_FALSE))));
        queryConfig.addSort(new DbSort(Customer_.NAME, DbSort.SortDir.ASC));
        queryConfig.setLimit(10);
        String json = mapper.writeValueAsString(queryConfig);
        QueryConfig queryConfigNew = mapper.readValue(json, QueryConfig.class);
        assertEquals(queryConfigNew.toString(), queryConfig.toString());
    }

    @Test
    public void testQueryConfigRequest_emptyQueryConfig() throws IOException {
        QueryConfig queryConfig = new QueryConfig();
        String json = new ObjectMapper().writeValueAsString(queryConfig);
        QueryConfig queryConfigNew = mapper.readValue(json, QueryConfig.class);
        assertEquals(queryConfigNew.toString(), queryConfig.toString());
    }
}
