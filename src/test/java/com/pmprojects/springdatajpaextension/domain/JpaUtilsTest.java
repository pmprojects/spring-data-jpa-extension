package com.pmprojects.springdatajpaextension.domain;

import com.pmprojects.springdatajpaextension.entity.*;
import com.pmprojects.springdatajpaextension.domain.DbSort.SortDir;
import com.pmprojects.springdatajpaextension.repository.CustomerRepository;
import com.pmprojects.springdatajpaextension.repository.DatabaseTestCreation;
import com.pmprojects.springdatajpaextension.repository.JpaExtendedRepositoryImpl;
import com.pmprojects.springdatajpaextension.repository.ProductRepository;
import com.pmprojects.springdatajpaextension.utils.AttributeUtils;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.persistence.criteria.Predicate.BooleanOperator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@EnableJpaRepositories(basePackages = { "com.pmprojects.springdatajpaextension.repository" }, repositoryBaseClass = JpaExtendedRepositoryImpl.class)
public class JpaUtilsTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private ProductRepository productRepository;

    private static LocalDateTime now;

    @BeforeAll
    public static void setUpClass() {
        now = LocalDateTime.now();
    }

    @BeforeEach
    public void setUp() {
        DatabaseTestCreation.setUp(entityManager.getEntityManager());
    }
    
    @Test
    public void queryConfig_builderDefault() {
    	QueryConfig queryConfig = QueryConfig.builder().build();
    	assertNotNull(queryConfig.getFilters());
    	assertNotNull(queryConfig.getSorts());
    	assertNotNull(queryConfig.getOffset());
    	assertNull(queryConfig.getLimit());
    }
    
    @Test
    public void queryConfig_builder() {
    	int limit = 1;
    	int offset = 1;
    	QueryConfig queryConfig = QueryConfig.builder()
    			.limit(limit)
    			.filter(DbFilterField.builder().build())
    			.sort(DbSort.builder().build())
    			.offset(offset)
    			.build();
    	assertEquals(1, queryConfig.getFilters().size());
    	assertEquals(1, queryConfig.getSorts().size());
    	assertEquals(offset, queryConfig.getOffset());
    	assertEquals(limit, queryConfig.getLimit());
    }
    
    @Test
    public void queryConfig_builderClear() {
    	int limit = 1;
    	int offset = 1;
    	QueryConfig queryConfig = QueryConfig.builder()
    			.limit(limit)
    			.filter(DbFilterField.builder().build())
    			.sort(DbSort.builder().build())
    			.offset(offset)
    			.clearFilters()
    			.clearSorts()
    			.build();
    	assertEquals(0, queryConfig.getFilters().size());
    	assertEquals(0, queryConfig.getSorts().size());
    	assertEquals(offset, queryConfig.getOffset());
    	assertEquals(limit, queryConfig.getLimit());
    }
    
    @Test
    public void dbFilterField_builderDefault() {
    	DbFilterField dbField = DbFilterField.builder().build();
    	assertNull(dbField.getArgument1());
    	assertNull(dbField.getArgument2());
    	assertNull(dbField.getFieldName());
    	assertNull(dbField.getOperator());
    }
    
    @Test
    public void dbFilterField_builder() {
    	String fieldName = "";
    	String argument1 = "";
    	String argument2 = "";
    	DbFilterOperator dbFilterOperator = DbFilterOperator.EQUALS;
    	
    	DbFilterField dbField = DbFilterField.builder()
    			.fieldName(fieldName)
    			.argument1(fieldName)
    			.argument2(argument2)
    			.operator(dbFilterOperator)
    			.build();
    	assertEquals(argument1, dbField.getArgument1());
    	assertEquals(argument2, dbField.getArgument2());
    	assertEquals(fieldName, dbField.getFieldName());
    	assertEquals(dbFilterOperator, dbField.getOperator());
    }
    
    @Test
    public void dbFilterComposite_builderDefault() {
    	DbFilterComposite dbField = DbFilterComposite.builder().build();
    	assertNull(dbField.getOperator());
    	assertNotNull(dbField.getFilters());
    }
    
    @Test
    public void dbFilterComposite_builder() {
    	BooleanOperator operator = BooleanOperator.AND;
    	DbFilterComposite dbField = DbFilterComposite.builder()
    			.operator(operator)
    			.filter(DbFilterField.builder().build())
    			.build();
    	assertEquals(operator, dbField.getOperator());
    	assertEquals(1, dbField.getFilters().size());
    }
    
    @Test
    public void dbFilterComposite_builderClear() {
    	BooleanOperator operator = BooleanOperator.AND;
    	DbFilterComposite dbField = DbFilterComposite.builder()
    			.operator(operator)
    			.filter(DbFilterField.builder().build())
    			.clearFilters()
    			.build();
    	assertEquals(operator, dbField.getOperator());
    	assertEquals(0, dbField.getFilters().size());
    }
    
    @Test
    public void whenFindByName_thenReturnCustomer() {
        Customer found = customerRepository.findByName("Mary");
        assertEquals("Mary", found.getName());
    }

    @Test
    public void testFilter() {
        QueryConfig queryConfig = new QueryConfig();
        Page<Customer> customers = customerRepository.find(queryConfig);
        assertEquals(customerRepository.findAll().size(), customers.getNumberOfElements());
    }
    
    @Test
    public void testCount() {
        QueryConfig queryConfig = new QueryConfig();
        long customers = customerRepository.count(queryConfig);
        assertEquals(customerRepository.count(), customers);
    }

    @Test
    public void testRepositoryFilter_basic() {
        QueryConfig queryConfig = new QueryConfig();
        List<Customer> customers = customerRepository.find(queryConfig).getContent();

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        criteriaQuery.from(Customer.class);
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertEquals(query.getResultList().size(), customers.size());
    }
    
    @Test
    public void testRepositoryFilter_duplicateFilter() {
        QueryConfig queryConfig = new QueryConfig();
        int id = 1;
        queryConfig.addFilter(new DbFilterField(Customer_.ID, DbFilterOperator.EQUALS, id));
        queryConfig.addFilter(new DbFilterField(Customer_.ID, DbFilterOperator.EQUALS, id));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().equal(root.get(Customer_.ID), id));

        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertEquals(query.getResultList().size(), customers.size());
    }

    @Test
    public void testRepositoryFilterField_orderDesc() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.getSorts().add(new DbSort(Customer_.ID, SortDir.DESC));
        List<Long> customers = customerRepository.filterField(queryConfig, Customer_.id);
        System.out.println(customers);

        CriteriaQuery<Long> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Long.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        Path<Long> pathId = root.get(Customer_.id);
        criteriaQuery.select(pathId);
        criteriaQuery.orderBy(entityManager.getEntityManager().getCriteriaBuilder().desc(pathId));
        TypedQuery<Long> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        List<Long> expectedIds = query.getResultList();
        assertEquals(expectedIds.size(), customers.size());
        assertTrue(customers.containsAll(expectedIds));
        assertTrue(expectedIds.containsAll(customers));
        assertEquals(expectedIds, customers);
        for (int i = 0; i < customers.size() - 1; i++) {
            assertTrue(customers.get(i) >= customers.get(i + 1));
        }
    }

    @Test
    public void testRepositoryFilterField_orderAsc() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.getSorts().add(new DbSort(Customer_.ID, SortDir.ASC));
        List<Long> customers = customerRepository.filterField(queryConfig, Customer_.id);
        System.out.println(customers);

        CriteriaQuery<Long> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Long.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        Path<Long> pathId = root.get(Customer_.id);
        criteriaQuery.select(pathId);
        criteriaQuery.orderBy(entityManager.getEntityManager().getCriteriaBuilder().asc(pathId));
        TypedQuery<Long> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        List<Long> expectedIds = query.getResultList();
        assertEquals(expectedIds.size(), customers.size());
        assertTrue(customers.containsAll(expectedIds));
        assertTrue(expectedIds.containsAll(customers));
        assertEquals(expectedIds, customers);
        for (int i = 0; i < customers.size() - 1; i++) {
            assertTrue(customers.get(i) <= customers.get(i + 1));
        }
    }

    @Test
    public void testRepositoryFilter_limit() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.setLimit(1);
        List<Customer> customers = customerRepository.find(queryConfig).getContent();

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        criteriaQuery.from(Customer.class);
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        query.setMaxResults(1);
        assertTrue(customers.size() <= 1);
        assertEquals(query.getResultList().size(), customers.size());
    }

    @Test
    public void testRepositoryFilter_contain() {
        String customerName = "h";

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.NAME), DbFilterOperator.CONTAINS, customerName));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().like(root.get(Customer_.NAME), "%" + customerName + "%"));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getName().contains(customerName)));
    }

    @Test
    public void testRepositoryFilter_notContain() {
        String customerName = "h";

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.NAME), DbFilterOperator.NOT_CONTAINS, customerName));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().notLike(root.get(Customer_.NAME), "%" + customerName + "%"));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().noneMatch(customer -> customer.getName().contains(customerName)));
    }

    @Test
    public void testRepositoryFilter_in() {
        String customerName = "John";
        List<String> customerNames = Collections.singletonList(customerName);

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.NAME), DbFilterOperator.IN, customerNames));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(root.get(Customer_.NAME).in(customerNames));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customerNames.contains(customer.getName())));
    }

    @Test
    public void testRepositoryFilter_notIn() {
        String customerName = "John";
        List<String> customerNames = Collections.singletonList(customerName);

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.NAME), DbFilterOperator.NOT_IN, customerNames));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(root.get(Customer_.NAME).in(customerNames).not());
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().noneMatch(customer -> customerNames.contains(customer.getName())));
    }

    @Test
    public void testRepositoryFilter_lessThan() {
        String customerName = "Manu";

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.NAME), DbFilterOperator.LESS_THAN, customerName));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().lessThan(root.get(Customer_.NAME), customerName));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getName().compareTo(customerName) < 0));
    }

    @Test
    public void testRepositoryFilter_greaterThan() {
        String customerName = "Manu";

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.NAME), DbFilterOperator.GREATER_THAN, customerName));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().greaterThan(root.get(Customer_.NAME), customerName));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getName().compareTo(customerName) > 0));
    }

    @Test
    public void testRepositoryFilter_lessOrEqualThan() {
        String customerName = "John";

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.NAME), DbFilterOperator.LESS_OR_EQUALS_THAN, customerName));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().lessThanOrEqualTo(root.get(Customer_.NAME), customerName));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getName().compareTo(customerName) <= 0));
    }

    @Test
    public void testRepositoryFilter_greaterOrEqualThan() {
        String customerName = "Mary";

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.NAME), DbFilterOperator.GREATER_OR_EQUALS_THAN, customerName));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().greaterThanOrEqualTo(root.get(Customer_.NAME), customerName));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getName().compareTo(customerName) >= 0));
    }

    @Test
    public void testRepositoryFilter_lt() {
        Double price = 5D;

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.price), DbFilterOperator.LESS_THAN, price));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().lt(root.get(Product_.PRICE), price));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getPrice() < price));
    }

    @Test
    public void testRepositoryFilter_gt() {
        Double price = 5D;

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.price), DbFilterOperator.GREATER_THAN, price));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().gt(root.get(Product_.PRICE), price));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getPrice() > price));
    }

    @Test
    public void testRepositoryFilter_le() {
        Double price = 5D;

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.price), DbFilterOperator.LESS_OR_EQUALS_THAN, price));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().le(root.get(Product_.PRICE), price));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getPrice() <= price));
    }

    @Test
    public void testRepositoryFilter_ge() {
        Double price = 5D;

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.PRICE), DbFilterOperator.GREATER_OR_EQUALS_THAN, price));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().ge(root.get(Product_.PRICE), price));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getPrice() >= price));
    }

    @Test
    public void testRepositoryFilter_isNull() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.PRICE), DbFilterOperator.IS_NULL));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().isNull(root.get(Product_.PRICE)));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getPrice() == null));
    }

    @Test
    public void testRepositoryFilter_isNotNull() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.PRICE), DbFilterOperator.IS_NOT_NULL));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().isNotNull(root.get(Product_.PRICE)));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getPrice() != null));
    }

    @Test
    public void testRepositoryFilter_isTrue() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.DELETED), DbFilterOperator.IS_TRUE));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().isTrue(root.get(Customer_.DELETED)));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(Customer::isDeleted));
    }

    @Test
    public void testRepositoryFilter_isFalse() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.DELETED), DbFilterOperator.IS_FALSE));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().isFalse(root.get(Customer_.DELETED)));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().noneMatch(Customer::isDeleted));
    }

    @Test
    public void testRepositoryFilter_betweenNumber() {
        Double left = 2D;
        Double right = 8D;
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.PRICE), DbFilterOperator.BETWEEN, left, right));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().between(root.get(Product_.PRICE), left, right));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getPrice() >= left && product.getPrice() <= right));
    }

    @Test
    public void testRepositoryFilter_betweenString() {
        String left = "M";
        String right = "P";
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.NAME), DbFilterOperator.BETWEEN, left, right));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().between(root.get(Product_.NAME), left, right));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getName().compareTo(left) >= 0 && product.getName().compareTo(right) <= 0));
    }

    @Test
    public void testRepositoryFilter_startsWith() {
        String productName = "M";

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.NAME), DbFilterOperator.STARTS_WITH, productName));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().like(root.get(Product_.NAME), productName + "%"));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertNotNull(products);
        assertTrue(products.size() > 0);
        assertEquals(3, products.size());
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getName().contains(productName)));
    }

    @Test
    public void testRepositoryFilter_startsWith_2() {
        String productName = "Mu";

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.NAME), DbFilterOperator.STARTS_WITH, productName));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().like(root.get(Product_.NAME), productName + "%"));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertNotNull(products);
        assertTrue(products.size() > 0);
        assertEquals(1, products.size());
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getName().contains(productName)));
    }

    @Test
    public void testRepositoryFilter_endsWith() {
        String productName = "om";

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.NAME), DbFilterOperator.ENDS_WITH, productName));
        List<Product> products = productRepository.find(queryConfig).getContent();
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().like(root.get(Product_.NAME), "%" + productName));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertNotNull(products);
        assertTrue(products.size() > 0);
        assertEquals(products.size(), 1);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getName().contains(productName)));
    }

    @Test
    public void testRepositoryFilter_samePath() {
        // Get all customers that have products products in list "Milk" and "Meat" and
        // product name contain "M"
        String productName1 = "Milk";
        String productName2 = "Meat";
        String productNameContains = "M";

        QueryConfig queryConfig = new QueryConfig();
        List<String> productsName = new ArrayList<>();
        productsName.add(productName1);
        productsName.add(productName2);
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.PRODUCTS, Product_.NAME), DbFilterOperator.IN, productsName));
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.PRODUCTS, Product_.NAME), DbFilterOperator.CONTAINS, productNameContains));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        criteriaQuery.distinct(true);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        Path<?> path = root.join(Customer_.PRODUCTS, JoinType.LEFT);
        criteriaQuery.where(path.get(Product_.NAME).in(productName1, productName2),
                entityManager.getEntityManager().getCriteriaBuilder().like(path.get(Product_.NAME), "%" + productNameContains + "%"));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getProducts().stream().anyMatch(product -> {
            boolean containsCorrect = product.getName().contains(productNameContains);
            boolean inCorrect = product.getName().equals(productName1) || product.getName().equals(productName2);
            return containsCorrect && inCorrect;
        })));
    }

    @Test
    public void testRepositoryFilter_sameRootPath() {
        // Get all customers that have product id in list {1L,2L} and
        // product name contain "M"
        Double productPrice1 = 10D;
        Double productPrice2 = 15D;
        String productNameContains = "M";

        QueryConfig queryConfig = new QueryConfig();
        List<Double> productsPrice = new ArrayList<>();
        productsPrice.add(productPrice1);
        productsPrice.add(productPrice2);
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.PRODUCTS, Product_.PRICE), DbFilterOperator.IN, productsPrice));
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.PRODUCTS, Product_.NAME), DbFilterOperator.CONTAINS, productNameContains));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        criteriaQuery.distinct(true);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        Path<?> path = root.join(Customer_.PRODUCTS, JoinType.LEFT);
        criteriaQuery.where(path.get(Product_.PRICE).in(productPrice1, productPrice2),
                entityManager.getEntityManager().getCriteriaBuilder().like(path.get(Product_.NAME), "%" + productNameContains + "%"));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getProducts().stream().anyMatch(product -> {
            boolean containsCorrect = product.getName().contains(productNameContains);
            boolean inCorrect = product.getPrice().equals(productPrice1) || product.getPrice().equals(productPrice2);
            return containsCorrect && inCorrect;
        })));
    }

    @Test
    public void testRepositoryOrder_asc() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addSort(new DbSort(Customer_.NAME, SortDir.ASC));
        Page<Customer> customers = customerRepository.find(queryConfig);
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.orderBy(entityManager.getEntityManager().getCriteriaBuilder().asc(root.get(Customer_.NAME)));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.getNumberOfElements() > 0);
        assertEquals(query.getResultList().size(), customers.getNumberOfElements());
        for (int i = 0; i < customers.getNumberOfElements() - 1; i++) {
            assertTrue(customers.getContent().get(i).getName().compareTo(customers.getContent().get(i + 1).getName()) <= 0);
        }
    }

    @Test
    public void testRepositoryOrder_desc() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addSort(new DbSort(Customer_.NAME, SortDir.DESC));
        Page<Customer> customers = customerRepository.find(queryConfig);
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.orderBy(entityManager.getEntityManager().getCriteriaBuilder().desc(root.get(Customer_.NAME)));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.getNumberOfElements() > 0);
        assertEquals(query.getResultList().size(), customers.getNumberOfElements());
        for (int i = 0; i < customers.getNumberOfElements() - 1; i++) {
            assertTrue(customers.getContent().get(i).getName().compareTo(customers.getContent().get(i + 1).getName()) >= 0);
        }
    }

    @Test
    public void testRepositoryOrder_ascDeepThrow() {
        assertThrows(RuntimeException.class, () -> {
            QueryConfig queryConfig = new QueryConfig();
            queryConfig.addSort(new DbSort(AttributeUtils.toAttributeName(Customer_.PRODUCTS, Product_.NAME), SortDir.ASC));
            customerRepository.find(queryConfig);
        });
    }

    @Test
    public void testRepositoryOrder_descDeepThrow() {
        assertThrows(RuntimeException.class, () -> {
            QueryConfig queryConfig = new QueryConfig();
            queryConfig.addSort(new DbSort(AttributeUtils.toAttributeName(Customer_.PRODUCTS, Product_.NAME), SortDir.DESC));
            customerRepository.find(queryConfig);
        });
    }

    @Test
    public void testRepositoryOrder_ascDeep() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addSort(new DbSort(AttributeUtils.toAttributeName(Product_.CUSTOMER, Customer_.NAME), SortDir.ASC));
        Page<Product> products = productRepository.find(queryConfig);
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        Path<?> path = root.join(Product_.CUSTOMER, JoinType.LEFT);
        criteriaQuery.orderBy(entityManager.getEntityManager().getCriteriaBuilder().asc(path.get(Customer_.NAME)));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(products.getNumberOfElements() > 0);
        assertEquals(query.getResultList().size(), products.getNumberOfElements());
        for (int i = 0; i < products.getNumberOfElements() - 1; i++) {
            assertTrue(products.getContent().get(i).getCustomer().getName().compareTo(products.getContent().get(i + 1).getCustomer().getName()) <= 0);
        }
    }

    @Test
    public void testRepositoryOrder_descDeep() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addSort(new DbSort(AttributeUtils.toAttributeName(Product_.CUSTOMER, Customer_.NAME), SortDir.DESC));
        Page<Product> products = productRepository.find(queryConfig);
        System.out.println(products);

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        Path<?> path = root.join(Product_.CUSTOMER, JoinType.LEFT);
        criteriaQuery.orderBy(entityManager.getEntityManager().getCriteriaBuilder().desc(path.get(Customer_.NAME)));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(products.getNumberOfElements() > 0);
        assertEquals(query.getResultList().size(), products.getNumberOfElements());
        for (int i = 0; i < products.getNumberOfElements() - 1; i++) {
            assertTrue(products.getContent().get(i).getCustomer().getName().compareTo(products.getContent().get(i + 1).getCustomer().getName()) >= 0);
        }
    }

    @Test
    public void testRepositoryLimit_deepFilter() {
        String name = "a";
        int limit = 2;
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.setLimit(limit);
        queryConfig.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Product_.CUSTOMER, Customer_.NAME), DbFilterOperator.CONTAINS, name));
        List<Product> products = productRepository.find(queryConfig).getContent();

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        Path<?> path = root.join(Product_.CUSTOMER, JoinType.LEFT);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().like(path.get(Customer_.NAME), "%" + name + "%"));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        query.setMaxResults(limit);
        assertTrue(products.size() <= 2);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getCustomer().getName().contains(name)));
    }

    @Test
    public void testRepositoryDateFilter_greaterOrEqualThen() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.LAST_UPDATE, DbFilterOperator.GREATER_OR_EQUALS_THAN, now));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().greaterThanOrEqualTo(root.get(Customer_.LAST_UPDATE), now));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().noneMatch(customer -> customer.getLastUpdate().isBefore(now)));
    }

    @Test
    public void testRepositoryDateFilter_greaterThen() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.LAST_UPDATE, DbFilterOperator.GREATER_THAN, now));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().greaterThan(root.get(Customer_.LAST_UPDATE), now));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getLastUpdate().isAfter(now)));
    }

    @Test
    public void testRepositoryDateFilter_lessOrEqualThen() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.LAST_UPDATE, DbFilterOperator.LESS_OR_EQUALS_THAN, now));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().lessThanOrEqualTo(root.get(Customer_.LAST_UPDATE), now));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().noneMatch(customer -> customer.getLastUpdate().isAfter(now)));
    }

    @Test
    public void testRepositoryDateFilter_lessThen() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.LAST_UPDATE, DbFilterOperator.LESS_THAN, now));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().lessThan(root.get(Customer_.LAST_UPDATE), now));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getLastUpdate().isBefore(now)));
    }

    @Test
    public void testRepositoryDateFilter_between() {
        LocalDateTime month = now.plusMonths(1);

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.LAST_UPDATE, DbFilterOperator.BETWEEN, now, month));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().between(root.get(Customer_.LAST_UPDATE), now, month));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> !customer.getLastUpdate().isBefore(now) && !customer.getLastUpdate().isAfter(month)));
    }

    @Test
    public void testRepositoryLocalDateFilter_greaterOrEqualThen() {
        LocalDate now = LocalDate.now();
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.REGISTRATION_DATE, DbFilterOperator.GREATER_OR_EQUALS_THAN, now));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().greaterThanOrEqualTo(root.get(Customer_.REGISTRATION_DATE), now));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().noneMatch(customer -> customer.getRegistrationDate().isBefore(now)));
    }

    @Test
    public void testRepositoryLocalDateFilter_greaterThen() {
        LocalDate now = LocalDate.now();
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.REGISTRATION_DATE, DbFilterOperator.GREATER_THAN, now));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().greaterThan(root.get(Customer_.REGISTRATION_DATE), now));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getRegistrationDate().isAfter(now)));
    }

    @Test
    public void testRepositoryLocalDateFilter_lessOrEqualThen() {
        LocalDate now = LocalDate.now();
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.REGISTRATION_DATE, DbFilterOperator.LESS_OR_EQUALS_THAN, now));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().lessThanOrEqualTo(root.get(Customer_.REGISTRATION_DATE), now));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().noneMatch(customer -> customer.getRegistrationDate().isAfter(now)));
    }

    @Test
    public void testRepositoryLocalDateFilter_lessThen() {
        LocalDate now = LocalDate.now();
        LocalDate month = now.plusMonths(1);
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.REGISTRATION_DATE, DbFilterOperator.LESS_THAN, month));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().lessThan(root.get(Customer_.REGISTRATION_DATE), month));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> customer.getRegistrationDate().isBefore(month)));
    }

    @Test
    public void testRepositoryLocalDateFilter_between() {
        LocalDate now = LocalDate.now();
        LocalDate month = now.plusMonths(1);

        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Customer_.REGISTRATION_DATE, DbFilterOperator.BETWEEN, now, month));
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().between(root.get(Customer_.REGISTRATION_DATE), now, month));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> !customer.getRegistrationDate().isBefore(now) && !customer.getRegistrationDate().isAfter(month)));
    }

    @Test
    public void testRepositoryFilterEnumField_equalString() {
        String enumField = EnumField.FIRST.name();
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Product_.ENUM_FIELD, DbFilterOperator.EQUALS, enumField));
        List<Product> products = productRepository.find(queryConfig).getContent();

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().equal(root.get(Product_.ENUM_FIELD), EnumField.FIRST));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getEnumField().name().equals(enumField)));
    }

    @Test
    public void testRepositoryFilterEnumField_equalEnum() {
        EnumField enumField = EnumField.FIRST;
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Product_.ENUM_FIELD, DbFilterOperator.EQUALS, enumField));
        List<Product> products = productRepository.find(queryConfig).getContent();

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().equal(root.get(Product_.ENUM_FIELD), enumField));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> product.getEnumField().equals(enumField)));
    }

    @Test
    public void testRepositoryFilterEnumField_notEqualString() {
        String enumField = EnumField.FIRST.name();
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Product_.ENUM_FIELD, DbFilterOperator.NOT_EQUALS, enumField));
        List<Product> products = productRepository.find(queryConfig).getContent();

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().notEqual(root.get(Product_.ENUM_FIELD), EnumField.FIRST));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().noneMatch(product -> product.getEnumField().name().equals(enumField)));
    }

    @Test
    public void testRepositoryFilterEnumField_notEqualEnum() {
        EnumField enumField = EnumField.FIRST;
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Product_.ENUM_FIELD, DbFilterOperator.NOT_EQUALS, enumField));
        List<Product> products = productRepository.find(queryConfig).getContent();

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().notEqual(root.get(Product_.ENUM_FIELD), enumField));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().noneMatch(product -> product.getEnumField().equals(enumField)));
    }

    @Test
    public void testRepositoryFilterEnumField_inListString() {
        List<String> enumFieldString = Arrays.asList(EnumField.FIRST.name(), EnumField.SECOND.name());
        List<EnumField> enumField = Arrays.asList(EnumField.FIRST, EnumField.SECOND);
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Product_.ENUM_FIELD, DbFilterOperator.IN, enumFieldString));
        List<Product> products = productRepository.find(queryConfig).getContent();

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(root.get(Product_.ENUM_FIELD).in(enumField));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> enumField.contains(product.getEnumField())));
    }

    @Test
    public void testRepositoryFilterEnumField_inListEnum() {
        List<EnumField> enumField = Arrays.asList(EnumField.FIRST, EnumField.SECOND);
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField(Product_.ENUM_FIELD, DbFilterOperator.IN, enumField));
        List<Product> products = productRepository.find(queryConfig).getContent();

        CriteriaQuery<Product> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);
        criteriaQuery.where(root.get(Product_.ENUM_FIELD).in(enumField));
        TypedQuery<Product> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        assertTrue(products.size() > 0);
        assertEquals(query.getResultList().size(), products.size());
        assertTrue(products.stream().allMatch(product -> enumField.contains(product.getEnumField())));
    }

    @Test
    public void testRepositoryFilterEmbeddedField() {
        QueryConfig queryConfig = new QueryConfig();
        queryConfig.addFilter(new DbFilterField("descriptions.localizedId.locale", DbFilterOperator.EQUALS, "IT"));
        List<Product> products = productRepository.find(queryConfig).getContent();
        assertEquals(products.size(), 1);
    }

    @Test
    public void testRepositoryFilter_and() {
        String customerName = "John";
        String productName = "Mushroom";

        QueryConfig queryConfig = new QueryConfig();
        DbFilterComposite and = new DbFilterComposite(Predicate.BooleanOperator.AND);
        and.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.NAME), DbFilterOperator.EQUALS, customerName));
        and.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.PRODUCTS, Product_.NAME), DbFilterOperator.EQUALS, productName));
        queryConfig.addFilter(and);
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        Path<?> path = root.join(Customer_.PRODUCTS, JoinType.LEFT);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().and(
                entityManager.getEntityManager().getCriteriaBuilder().equal(root.get(Customer_.NAME), customerName),
                entityManager.getEntityManager().getCriteriaBuilder().equal(path.get(Product_.NAME), productName)));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);

        assertTrue(customers.size() > 0);
        assertEquals(query.getResultList().size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> {
            boolean customerNameCorrect = customer.getName().equals(customerName);
            boolean productNameCorrect = customer.getProducts() != null && customer.getProducts().stream().anyMatch(product -> product.getName().equals(productName));
            return customerNameCorrect && productNameCorrect;
        }));
    }

    @Test
    public void testRepositoryFilter_or() {
        String customerName = "John";
        String productName = "Mushroom";

        QueryConfig queryConfig = new QueryConfig();
        DbFilterComposite and = new DbFilterComposite(Predicate.BooleanOperator.OR);
        and.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.NAME), DbFilterOperator.EQUALS, customerName));
        and.addFilter(new DbFilterField(AttributeUtils.toAttributeName(Customer_.PRODUCTS, Product_.NAME), DbFilterOperator.EQUALS, productName));
        queryConfig.addFilter(and);
        List<Customer> customers = customerRepository.find(queryConfig).getContent();
        System.out.println(customers);

        CriteriaQuery<Customer> criteriaQuery = entityManager.getEntityManager().getCriteriaBuilder().createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        Path<?> path = root.join(Customer_.PRODUCTS, JoinType.LEFT);
        criteriaQuery.distinct(true);
        criteriaQuery.where(entityManager.getEntityManager().getCriteriaBuilder().or(
                entityManager.getEntityManager().getCriteriaBuilder().equal(root.get(Customer_.NAME), customerName),
                entityManager.getEntityManager().getCriteriaBuilder().equal(path.get(Product_.NAME), productName)));
        TypedQuery<Customer> query = entityManager.getEntityManager().createQuery(criteriaQuery);
        List<Customer> expectedCustomers = query.getResultList();

        assertTrue(customers.size() > 0);
        assertEquals(expectedCustomers.size(), customers.size());
        assertTrue(customers.stream().allMatch(customer -> {
            boolean customerNameCorrect = customer.getName().equals(customerName);
            boolean productNameCorrect = customer.getProducts() != null && customer.getProducts().stream().anyMatch(product -> product.getName().equals(productName));
            return customerNameCorrect || productNameCorrect;
        }));
    }
}
