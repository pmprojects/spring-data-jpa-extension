package com.pmprojects.springdatajpaextension.repository;


import com.pmprojects.springdatajpaextension.entity.Product;

public interface ProductRepository extends JpaExtendedRepository<Product, Long> {

}
