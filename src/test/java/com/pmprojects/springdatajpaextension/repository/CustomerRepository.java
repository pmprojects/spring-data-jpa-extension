package com.pmprojects.springdatajpaextension.repository;

import com.pmprojects.springdatajpaextension.entity.Customer;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaExtendedRepository<Customer, Long> {

    Customer findByName(String name);
}
