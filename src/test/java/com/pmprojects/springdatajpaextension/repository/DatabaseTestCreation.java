package com.pmprojects.springdatajpaextension.repository;

import com.pmprojects.springdatajpaextension.entity.*;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;

public class DatabaseTestCreation {

    public static void setUp(EntityManager em) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        
        Customer mary = new Customer("Mary");
        Product meat = new Product("Meat", 10D, EnumField.FIRST);
        ProductLocalized productLocalized = new ProductLocalized();
        productLocalized.setLocalizedId(new ProductLocalizedId("IT"));
        productLocalized.setDescription("Descrizione carne");
        productLocalized.setProduct(meat);
        
        ProductLocalized productLocalizedEn = new ProductLocalized();
        productLocalizedEn.setLocalizedId(new ProductLocalizedId("EN"));
        productLocalizedEn.setDescription("meat description");
        productLocalizedEn.setProduct(meat);
        meat.addDescription("IT", productLocalized);
        meat.addDescription("EN", productLocalizedEn);
        mary.addProduct(meat);
        mary.addProduct(new Product("Milk", 5D, EnumField.SECOND));
        mary.setLastUpdate(LocalDateTime.now().minusDays(1));
        mary.setRegistrationDate(LocalDate.now());
        Customer john = new Customer("John");
        john.addProduct(new Product("Mushroom", 2D));
        john.addProduct(new Product("Water", null));
        john.setLastUpdate(LocalDateTime.now().plusDays(1));
        john.setRegistrationDate(LocalDate.now().plusMonths(1));
        Customer deleted = new Customer("John");
        deleted.setDeleted(true);
        em.persist(mary);
        em.persist(john);
        em.persist(deleted);
    }
    
    public static void setUpWithTransaction(EntityManager em) {
        em.getTransaction().begin();
        setUp(em);
        em.getTransaction().commit();
    }
}
