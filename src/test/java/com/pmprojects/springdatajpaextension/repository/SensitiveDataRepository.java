package com.pmprojects.springdatajpaextension.repository;

import com.pmprojects.springdatajpaextension.entity.SensitiveData;

public interface SensitiveDataRepository extends JpaExtendedRepository<SensitiveData, Long> {

}
