package com.pmprojects.springdatajpaextension.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = Customer.CUSTOMER)
@Getter
@Setter
@NoArgsConstructor
public class Customer {
    public static final String CUSTOMER = "customer";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
    private Set<Product> products;

    private String name;
    
    @Column(nullable = false)
    private boolean deleted = false;

    private LocalDate registrationDate;
    
    private LocalDateTime lastUpdate;

    public Customer(String name) {
        this(name, null);
    }

    public Customer(String name, Set<Product> products) {
        this();
        this.name = name;
        this.products = products;
    }

    public void addProduct(Product product) {
        if (products == null) {
            products = new HashSet<>();
        }
        product.setCustomer(this);
        products.add(product);
    }

    @Override
    public String toString() {
        return "Customer [id=" + id + ", name=" + name + "]";
    }
}
