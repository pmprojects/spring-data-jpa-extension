package com.pmprojects.springdatajpaextension.entity;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ProductLocalizedId implements Serializable {

	private Long id;
	private String locale;

    public ProductLocalizedId(String locale) {
        this.locale = locale;
    }
}
