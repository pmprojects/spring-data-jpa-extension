package com.pmprojects.springdatajpaextension.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = Product.PRODUCT)
@Getter @Setter @NoArgsConstructor
public class Product {
    public static final String PRODUCT = "product";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_customer", nullable = false)
    private Customer customer;

    private String name;
    
    private Double price;

    private EnumField enumField;
    
    @OneToMany(mappedBy = "product", cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
    @MapKey(name = "localizedId.locale")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Map<String, ProductLocalized> descriptions = new HashMap<>();

    public Product(String name, Double price) {
        this(name, price, null);
    }
    
    public Product(String name, Double price, EnumField enumField) {
        this.name = name;
        this.price = price;
        this.enumField = enumField;
    }

    public void addDescription(String locale, ProductLocalized description) {
        descriptions.put(locale, description);
    }

    @Override
    public String toString() {
        return "Product [id=" + id + ", customer=" + customer + ", name=" + name + ", price=" + price + ", enumField=" + enumField + "]";
    }
}
