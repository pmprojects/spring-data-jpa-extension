package com.pmprojects.springdatajpaextension.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name = ProductLocalized.PRODUCT_LOCALIZED)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ProductLocalized {

    public static final String PRODUCT_LOCALIZED = "product_localized";
    
    @EmbeddedId
    private ProductLocalizedId localizedId;
    
    private String description;
    
    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "id")
    private Product product;
    
}
