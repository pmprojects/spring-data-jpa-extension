package com.pmprojects.springdatajpaextension.entity;

import com.pmprojects.springdatajpaextension.crypto.converter.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = SensitiveData.SENSITIVE_DATA)
@Getter
@Setter
@NoArgsConstructor
@ToString
public class SensitiveData {

    public static final String SENSITIVE_DATA = "sensitive_data";

    @Id
    private Long id;

    @Convert(converter = StringJpaCryptoConverter.class)
    private String sensitiveString;

    @Convert(converter = LocalDateJpaCryptoConverter.class)
    private LocalDate sensitiveLocalDate;

    @Convert(converter = IntegerJpaCryptoConverter.class)
    private Integer sensitiveInteger;

    @Convert(converter = LongJpaCryptoConverter.class)
    private Long sensitiveLong;

    @Convert(converter = DoubleJpaCryptoConverter.class)
    private Double sensitiveDouble;
}
