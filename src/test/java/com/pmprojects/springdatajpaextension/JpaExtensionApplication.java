package com.pmprojects.springdatajpaextension;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaExtensionApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaExtensionApplication.class, args);
	}

}
